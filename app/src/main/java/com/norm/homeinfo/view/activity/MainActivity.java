package com.norm.homeinfo.view.activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.norm.homeinfo.R;
import com.norm.homeinfo.adapter.MyViewPagerAdapter;
import com.norm.homeinfo.view.activity.auth.AuthActivity;
import com.norm.homeinfo.view.fragment.owner.ManageRoomFragment;
import com.norm.homeinfo.view.fragment.user.HomeExpenditureFragment;
import com.norm.homeinfo.view.fragment.user.RoomListFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = MainActivity.class.getSimpleName();

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.bottomNav)
    BottomNavigationView mBottomNavView;

    @BindView(R.id.view_pager)
    ViewPager mViewPager;

    @BindView(R.id.toolbar_title)
    AppCompatTextView toolBarTitle;

    @BindView(R.id.toolbar_lbl_action_owner)
    AppCompatImageView toolBarActionOwner;

    MenuItem mPrevMenuItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // ButterKnife
        setUpButterKnife();

        // ToolBar
        setUpToolBar();

        // BottomNavigationView
        setUpBottomNavView();

        // ViewPager
        setUpViewPager();

    }

    // ButterKnife
    private void setUpButterKnife() {
        ButterKnife.bind(this);
    }

    // ToolBar
    private void setUpToolBar() {
        if (toolbar != null) {
            toolbar.setTitle("Search");
            Log.d(TAG, "Title: Search");
        }

        toolBarActionOwner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, AuthActivity.class);
                startActivity(i);
                Log.d(TAG, "Go to " + AuthActivity.class.getSimpleName());
            }
        });
    }

    // BottomNavigationView
    private void setUpBottomNavView() {
        mBottomNavView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                //TODO Change Your Fragment Page
                switch (menuItem.getItemId()) {
                    case R.id.bottom_nav_home:
                        mViewPager.setCurrentItem(0);
                        toolBarTitle.setText("Home");
                        break;
                    case R.id.bottom_nav_active_user:
                        mViewPager.setCurrentItem(1);
                        toolBarTitle.setText("Home");
                        break;
                    case R.id.bottom_nav_pending:
                        mViewPager.setCurrentItem(2);
                        toolBarTitle.setText("Home");
                        break;
                }
                return false;
            }
        });
    }

    // ViewPager
    private void setUpViewPager() {
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {
                Log.d(TAG, "Page Scrolled");
            }

            @Override
            public void onPageSelected(int i) {
                Log.d(TAG, "Page Selected");

                if (mPrevMenuItem != null) {
                    mPrevMenuItem.setChecked(false);
                } else {
                    mBottomNavView.getMenu().getItem(0).setChecked(false);
                }

                Log.d(TAG, "Fragment " + i);
                mBottomNavView.getMenu().getItem(i).setChecked(true);
                mPrevMenuItem = mBottomNavView.getMenu().getItem(i);
                toolBarTitle.setText("Home");
            }

            @Override
            public void onPageScrollStateChanged(int i) {
                Log.d(TAG, "Page Scroll State Changed");
            }
        });

        loadFragment();
    }

    // Load Fragment
    private void loadFragment() {
        //TODO initialize your fragment and use addFragment(fragment) method to add fragment

        MyViewPagerAdapter pagerAdapter = new MyViewPagerAdapter(getSupportFragmentManager());
        pagerAdapter.addFragment(new ManageRoomFragment(), "NAV1");
        pagerAdapter.addFragment(new RoomListFragment(), "NAV2");
        pagerAdapter.addFragment(new HomeExpenditureFragment(), "NAV3");
        mViewPager.setAdapter(pagerAdapter);
    }
}
