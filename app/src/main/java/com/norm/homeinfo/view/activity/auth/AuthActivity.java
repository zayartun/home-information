package com.norm.homeinfo.view.activity.auth;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.norm.homeinfo.R;
import com.norm.homeinfo.utils.network.MyConnectionState;
import com.norm.homeinfo.view.activity.OwnerActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AuthActivity extends AppCompatActivity {

    public static final String TAG = AuthActivity.class.getSimpleName();

    @BindView(R.id.auth_layout)
    ConstraintLayout authLayout;

    @BindView(R.id.txt_owner_email)
    AppCompatEditText txtOwnerEmail;

    @BindView(R.id.txt_owner_password)
    AppCompatEditText txtOwnerPass;

    @BindView(R.id.btn_owner_login)
    AppCompatButton btnOwnerLogin;

    FirebaseAuth mFirebaseAuth;

    String ownerEmail = "";
    String ownerPass = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // FullScreen Mode
        setUpFullScreen();

        setContentView(R.layout.activity_auth);

        // ButterKnife
        setUpButterKnife();

        mFirebaseAuth = FirebaseAuth.getInstance();

        btnOwnerLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ownerLogin();
            }
        });

    }

    // FullScreen Mode
    private void setUpFullScreen() {
        // Theme
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }

        // Fullscreen
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    // ButterKnife
    private void setUpButterKnife() {
        ButterKnife.bind(this);
    }

    // Owner Login
    public void ownerLogin() {

        if (!TextUtils.isEmpty(txtOwnerEmail.getText())
                && !TextUtils.isEmpty(txtOwnerPass.getText())) {
            ownerEmail = txtOwnerEmail.getText().toString().toLowerCase().trim();
            ownerPass = txtOwnerPass.getText().toString().trim();
        } else {
            showError("Email and password didn't match!");
        }

        if (MyConnectionState.checkNetworkState(AuthActivity.this)) {
            doLogIn();
        } else {
            showError("No internet access");
        }

    }

    public void doLogIn() {
        mFirebaseAuth.signInWithEmailAndPassword(ownerEmail, ownerPass)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "Login Successful");
                            finish();
                            Intent i = new Intent(AuthActivity.this, OwnerActivity.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                        } else {
                            showError("Something went wrong!");
                            txtOwnerPass.requestFocus();
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e(TAG, e.getMessage());

                        showError("Something went wrong!");
                        txtOwnerPass.requestFocus();
                    }
                });
    }

    private void showError(String msg) {
        Snackbar snackbar = Snackbar.make(authLayout, msg, BaseTransientBottomBar.LENGTH_SHORT);
        snackbar.show();
    }

    @Override
    protected void onStart() {
        super.onStart();

        FirebaseUser currentUser = mFirebaseAuth.getCurrentUser();
        if (currentUser != null) {
            Log.d(TAG, "User : " + currentUser.getEmail());
            startActivity(new Intent(AuthActivity.this, OwnerActivity.class));
            finish();
        }
    }
}
