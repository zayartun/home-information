package com.norm.homeinfo.view.activity.room;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.norm.homeinfo.R;

public class RoomEditActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_room);
    }
}
