package com.norm.homeinfo.view.activity.room;

import android.support.constraint.ConstraintLayout;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.google.firebase.firestore.FirebaseFirestore;
import com.norm.homeinfo.R;
import com.norm.homeinfo.adapter.room.RoomMonthlyListAdapter;
import com.norm.homeinfo.network.model.RoomPayment;
import com.norm.homeinfo.utils.recyclerview.MyDividerItemDecoration;
import com.norm.homeinfo.utils.recyclerview.RecyclerTouchListener;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RoomDetailActivity extends AppCompatActivity {

    public static final String TAG = RoomDetailActivity.class.getSimpleName();

    @BindView(R.id.room_monthly_lists)
    RecyclerView mRoomMonthlyListView;

    @BindView(R.id.room_detail_layout)
    ConstraintLayout roomDetailLayout;

    List<RoomPayment> mRoomPaymentLists = new ArrayList<>();
    RoomMonthlyListAdapter mRoomMonthlyListAdapter;

    FirebaseFirestore mFirestore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_detail);

        mFirestore = FirebaseFirestore.getInstance();

        // ButterKnife
        setUpButterKnife();

        mRoomMonthlyListAdapter = new RoomMonthlyListAdapter(this);
        mRoomMonthlyListAdapter.setRoomMonthlyItemLists(mRoomPaymentLists);

        mRoomMonthlyListView.setLayoutManager(new LinearLayoutManager(this));
        mRoomMonthlyListView.setItemAnimator(new DefaultItemAnimator());
        mRoomMonthlyListView.addItemDecoration(new MyDividerItemDecoration(this, LinearLayoutManager.VERTICAL, 16));
        mRoomMonthlyListView.setAdapter(mRoomMonthlyListAdapter);

        mRoomMonthlyListView.addOnItemTouchListener(new RecyclerTouchListener(this, mRoomMonthlyListView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Snackbar.make(roomDetailLayout,
                        "Your meter unit price is " + mRoomPaymentLists.get(position).getMeter_price(),
                        BaseTransientBottomBar.LENGTH_SHORT).show();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        prepareData();

    }

    // ButterKnife
    private void setUpButterKnife() {
        ButterKnife.bind(this);
    }

    public void prepareData() {

        mRoomPaymentLists.clear();

        RoomPayment model = new RoomPayment(new Date(), 56, 3000, 0, 65000, 1);
        mRoomPaymentLists.add(model);

        model = new RoomPayment(new Date(), 56, 56 * 70, 0, 65000, 1);
        mRoomPaymentLists.add(model);

        model = new RoomPayment(new Date(), 30, 30 * 70, 1, 65000, 0);
        mRoomPaymentLists.add(model);

        model = new RoomPayment(new Date(), 200, 200 * 70, 0, 65000, 0);
        mRoomPaymentLists.add(model);

        model = new RoomPayment(new Date(), 130, 130 * 70, 1, 65000, 1);
        mRoomPaymentLists.add(model);

        model = new RoomPayment(new Date(), 56, 56 * 70, 0, 65000, 1);
        mRoomPaymentLists.add(model);

        model = new RoomPayment(new Date(), 30, 30 * 70, 1, 65000, 0);
        mRoomPaymentLists.add(model);

        model = new RoomPayment(new Date(), 200, 200 * 70, 0, 65000, 0);
        mRoomPaymentLists.add(model);

        model = new RoomPayment(new Date(), 130, 130 * 70, 1, 65000, 1);
        mRoomPaymentLists.add(model);

        model = new RoomPayment(new Date(), 56, 56 * 70, 0, 65000, 1);
        mRoomPaymentLists.add(model);

        model = new RoomPayment(new Date(), 30, 30 * 70, 1, 65000, 0);
        mRoomPaymentLists.add(model);

        model = new RoomPayment(new Date(), 200, 200 * 70, 0, 65000, 0);
        mRoomPaymentLists.add(model);

        model = new RoomPayment(new Date(), 130, 130 * 70, 1, 65000, 1);
        mRoomPaymentLists.add(model);

        mRoomMonthlyListAdapter.setRoomMonthlyItemLists(mRoomPaymentLists);

    }
}
