package com.norm.homeinfo.view.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.norm.homeinfo.R;
import com.norm.homeinfo.adapter.RoomListAdapter;
import com.norm.homeinfo.network.model.Room;
import com.norm.homeinfo.utils.recyclerview.RecyclerTouchListener;
import com.norm.homeinfo.view.activity.room.RoomDetailActivity;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OwnerActivity extends AppCompatActivity {

    public static final String TAG = OwnerActivity.class.getSimpleName();

    @BindView(R.id.owner_activity)
    ConstraintLayout ownerActivityLayout;

    @BindView(R.id.room_lists)
    RecyclerView mRoomListView;

    List<Room> rooms = new ArrayList<>();

    RoomListAdapter mRoomListAdapter;

    FirebaseFirestore mFirestore;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_owner);

        mFirestore = FirebaseFirestore.getInstance();

        // ButterKnife
        setUpButterKnife();

        mRoomListAdapter = new RoomListAdapter(this);
        mRoomListAdapter.setRoomItemLists(rooms);

        mRoomListView.setLayoutManager(new GridLayoutManager(this, 2, LinearLayoutManager.VERTICAL, false));
        mRoomListView.setItemAnimator(new DefaultItemAnimator());
        mRoomListView.setAdapter(mRoomListAdapter);

        mRoomListView.addOnItemTouchListener(new RecyclerTouchListener(this, mRoomListView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Intent i = new Intent(OwnerActivity.this, RoomDetailActivity.class);
                startActivity(i);
            }

            @Override
            public void onLongClick(View view, int position) {
                showActionsDialog(position);
            }
        }));

        prepareData();

    }

    // ButterKnife
    private void setUpButterKnife() {
        ButterKnife.bind(this);
    }

    private void prepareData() {
        mFirestore.collection("room").orderBy("room_no")
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.w(TAG, "Listen failed.", e);
                            return;
                        }

                        rooms.clear();
                        for (QueryDocumentSnapshot snapshot : queryDocumentSnapshots) {

                            Room room = snapshot.toObject(Room.class).withId(snapshot.getId());
                            rooms.add(room);

                        }
                        mRoomListAdapter.setRoomItemLists(rooms);
                    }
                });
    }

    private void showActionsDialog(final int position) {
        final CharSequence[] options = new CharSequence[] { "Edit", "Delete" };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Choose Option");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == 0) {
                    Snackbar.make(ownerActivityLayout, "You click " + options[0], BaseTransientBottomBar.LENGTH_SHORT).show();
                } else {
                    Snackbar.make(ownerActivityLayout, "You click " + options[1], BaseTransientBottomBar.LENGTH_SHORT).show();
                }
            }
        });
        builder.show();
    }
}
