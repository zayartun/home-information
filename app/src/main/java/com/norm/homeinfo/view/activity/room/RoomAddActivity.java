package com.norm.homeinfo.view.activity.room;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.norm.homeinfo.R;

public class RoomAddActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_room);
    }
}
