package com.norm.homeinfo.view.fragment.owner;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.norm.homeinfo.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ManageRoomFragment extends Fragment {


    public ManageRoomFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_room, container, false);
    }

}
