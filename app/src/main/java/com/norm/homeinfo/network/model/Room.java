package com.norm.homeinfo.network.model;

import java.util.Date;
import java.util.List;

public class Room extends Model {

    private int room_no;
    private int building_no;
    private int no_of_people;
    private String remark;

    private List<RoomPayment> room_payments;

    private Date created_date;
    private Date modified_date;


    public Room() {
    }

    public Room(int room_no, int building_no, int no_of_people, String remark, Date modified_date) {
        this.room_no = room_no;
        this.building_no = building_no;
        this.no_of_people = no_of_people;
        this.remark = remark;
        this.modified_date = modified_date;
    }

    public Room(int room_no, int building_no, int no_of_people, String remark, Date created_date, Date modified_date) {
        this.room_no = room_no;
        this.building_no = building_no;
        this.no_of_people = no_of_people;
        this.remark = remark;
        this.created_date = created_date;
        this.modified_date = modified_date;
    }

    public Room(int room_no, int building_no, int no_of_people, String remark, List<RoomPayment> room_payments, Date modified_date) {
        this.room_no = room_no;
        this.building_no = building_no;
        this.no_of_people = no_of_people;
        this.remark = remark;
        this.room_payments = room_payments;
        this.modified_date = modified_date;
    }

    public int getRoom_no() {
        return room_no;
    }

    public int getBuilding_no() {
        return building_no;
    }

    public int getNo_of_people() {
        return no_of_people;
    }

    public String getRemark() {
        return remark;
    }

    public Date getCreated_date() {
        return created_date;
    }

    public Date getModified_date() {
        return modified_date;
    }

    public void setRoom_no(int room_no) {
        this.room_no = room_no;
    }

    public void setBuilding_no(int type) {
        this.building_no = type;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public void setNo_of_people(int no_of_people) {
        this.no_of_people = no_of_people;
    }

    public void setModified_date(Date modified_date) {
        this.modified_date = modified_date;
    }

    public void setCreated_date(Date created_date) {
        this.created_date = created_date;
    }

    public List<RoomPayment> getRoom_payments() {
        return room_payments;
    }

    public void setRoom_payments(List<RoomPayment> room_payments) {
        this.room_payments = room_payments;
    }
}
