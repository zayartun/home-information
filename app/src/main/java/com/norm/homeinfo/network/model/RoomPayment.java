package com.norm.homeinfo.network.model;

import java.util.Date;

public class RoomPayment extends Model {

    private Date date;
    private int meter_unit;
    private int meter_price;
    private int meter_payment;

    private int rent_price;
    private int rent_payment;

    public RoomPayment() {
    }

    public RoomPayment(Date date, int meter_unit, int meter_price, int meter_payment, int rent_price, int rent_payment) {
        this.date = date;
        this.meter_unit = meter_unit;
        this.meter_price = meter_price;
        this.meter_payment = meter_payment;
        this.rent_price = rent_price;
        this.rent_payment = rent_payment;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getMeter_unit() {
        return meter_unit;
    }

    public void setMeter_unit(int meter_unit) {
        this.meter_unit = meter_unit;
    }

    public int getMeter_price() {
        return meter_price;
    }

    public void setMeter_price(int meter_price) {
        this.meter_price = meter_price;
    }

    public int getMeter_payment() {
        return meter_payment;
    }

    public void setMeter_payment(int meter_payment) {
        this.meter_payment = meter_payment;
    }

    public int getRent_price() {
        return rent_price;
    }

    public void setRent_price(int rent_price) {
        this.rent_price = rent_price;
    }

    public int getRent_payment() {
        return rent_payment;
    }

    public void setRent_payment(int rent_payment) {
        this.rent_payment = rent_payment;
    }
}
