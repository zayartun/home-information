package com.norm.homeinfo.adapter;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class MyViewPagerAdapter extends FragmentPagerAdapter {

    private final List<Fragment> mFragmentLists = new ArrayList<>();
    private final List<String> mFragmentTitleList = new ArrayList<>();

    public MyViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {
        return mFragmentLists.get(i);
    }

    @Override
    public int getCount() {
        return mFragmentLists.size();
    }

    public void addFragment(Fragment fragment, String title) {
        this.mFragmentLists.add(fragment);
        this.mFragmentTitleList.add(title);
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mFragmentTitleList.get(position);
    }
}
