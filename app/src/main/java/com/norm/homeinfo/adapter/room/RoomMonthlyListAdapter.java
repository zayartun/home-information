package com.norm.homeinfo.adapter.room;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.norm.homeinfo.R;
import com.norm.homeinfo.network.model.RoomPayment;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RoomMonthlyListAdapter extends RecyclerView.Adapter<RoomMonthlyListAdapter.RoomMonthlyViewHolder> {

    Context mContext;
    List<RoomPayment> mRoomPaymentList;

    public RoomMonthlyListAdapter(Context mContext) {
        this.mContext = mContext;
    }

    public void setRoomMonthlyItemLists(List<RoomPayment> lists) {
        this.mRoomPaymentList = lists;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RoomMonthlyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.room_monthly_list_row, viewGroup, false);
        return new RoomMonthlyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RoomMonthlyViewHolder roomMonthlyViewHolder, int i) {
        RoomPayment roomPayment = mRoomPaymentList.get(i);
        roomMonthlyViewHolder.bindView(roomPayment);
    }

    @Override
    public int getItemCount() {
        return mRoomPaymentList.size();
    }

    class RoomMonthlyViewHolder extends RecyclerView.ViewHolder {

        RoomPayment roomPayment;

        @BindView(R.id.tv_meter_unit_price)
        AppCompatTextView tvMeterUnitPrice;

        public RoomMonthlyViewHolder(@NonNull View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);

        }

        public void bindView(RoomPayment roomPayment) {
            this.roomPayment = roomPayment;

            tvMeterUnitPrice.setText("" + roomPayment.getMeter_price());
        }
    }

}
