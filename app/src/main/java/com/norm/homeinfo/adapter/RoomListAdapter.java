package com.norm.homeinfo.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.norm.homeinfo.R;
import com.norm.homeinfo.network.model.Room;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RoomListAdapter extends RecyclerView.Adapter<RoomListAdapter.RoomViewHolder> {

    Context mContext;
    List<Room> mRoomLists;

    public RoomListAdapter(Context mContext) {
        this.mContext = mContext;
    }

    public void setRoomItemLists(List<Room> lists) {
        this.mRoomLists = lists;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RoomViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.room_list_row, viewGroup, false);
        return new RoomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RoomViewHolder roomViewHolder, int i) {
        Room room = mRoomLists.get(i);
        roomViewHolder.bindView(room);
    }

    @Override
    public int getItemCount() {
        return mRoomLists.size();
    }

    class RoomViewHolder extends RecyclerView.ViewHolder {

        Room room;

        @BindView(R.id.tv_room_no)
        AppCompatTextView tvRoomNo;

        public RoomViewHolder(@NonNull View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }

        public void bindView(Room room) {
            this.room = room;

            String str = mContext.getResources().getString(R.string.str_room) + " " + room.getRoom_no();
            tvRoomNo.setText(str);
        }
    }

}
